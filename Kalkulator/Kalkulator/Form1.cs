﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonplus_Click(object sender, EventArgs e)
        {
            double a, b, c;
            bool empty1 = false, dobparse1 = false;
            bool empty2 = false, dobparse2 = false;
            if (textBox1.Text== "") {
                empty1 = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }
            if (textBox2.Text == "")
            {
                empty2 = true;
                MessageBox.Show("Drugi broj je prazan.", "Pogreška!");
            }
            if (!empty1 && !empty2)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse1 = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!double.TryParse(textBox2.Text, out b) || textBox2.Text.Contains('.'))
                {
                    dobparse2 = true;
                    MessageBox.Show("Pogrešan unos drugog borja.", "Pogreška!");
                }
                if (!empty1 && !empty2 && !dobparse1 && !dobparse2) {
                    c = a + b;
                    textBox3.Text = c.ToString();
                }
            }
        }

        private void buttonminus_Click(object sender, EventArgs e)
        {
            double a, b, c;
            bool empty1 = false, dobparse1 = false;
            bool empty2 = false, dobparse2 = false;
            if (textBox1.Text == "")
            {
                empty1 = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }
            if (textBox2.Text == "")
            {
                empty2 = true;
                MessageBox.Show("Drugi broj je prazan.", "Pogreška!");
            }
            if (!empty1 && !empty2)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse1 = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!double.TryParse(textBox2.Text, out b) || textBox2.Text.Contains('.'))
                {
                    dobparse2 = true;
                    MessageBox.Show("Pogrešan unos drugog borja.", "Pogreška!");
                }
                if (!empty1 && !empty2 && !dobparse1 && !dobparse2)
                {
                    c = a - b;
                    textBox3.Text = c.ToString();
                }
            }
        }

        private void buttontimes_Click(object sender, EventArgs e)
        {
            double a, b, c;
            bool empty1 = false, dobparse1 = false;
            bool empty2 = false, dobparse2 = false;
            if (textBox1.Text == "")
            {
                empty1 = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }
            if (textBox2.Text == "")
            {
                empty2 = true;
                MessageBox.Show("Drugi broj je prazan.", "Pogreška!");
            }
            if (!empty1 && !empty2)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse1 = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!double.TryParse(textBox2.Text, out b) || textBox2.Text.Contains('.'))
                {
                    dobparse2 = true;
                    MessageBox.Show("Pogrešan unos drugog borja.", "Pogreška!");
                }
                if (!empty1 && !empty2 && !dobparse1 && !dobparse2)
                {
                    c = a * b;
                    textBox3.Text = c.ToString();
                }
            }
        }

        private void buttondiv_Click(object sender, EventArgs e)
        {
            double a, b, c;
            bool empty1 = false, dobparse1 = false;
            bool empty2 = false, dobparse2 = false;
            if (textBox1.Text == "")
            {
                empty1 = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }
            if (textBox2.Text == "")
            {
                empty2 = true;
                MessageBox.Show("Drugi broj je prazan.", "Pogreška!");
            }
            if (!empty1 && !empty2)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse1 = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!double.TryParse(textBox2.Text, out b) || textBox2.Text.Contains('.'))
                {
                    dobparse2 = true;
                    MessageBox.Show("Pogrešan unos drugog borja.", "Pogreška!");
                }
                if (!empty1 && !empty2 && !dobparse1 && !dobparse2)
                {
                    if (b!=0)
                    {
                        c = a / b;
                        textBox3.Text = c.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Drugi broj ne moze biti 0!", "Pogreška!");
                    }
                }
            }
        }

        private void buttonsin_Click(object sender, EventArgs e)
        {
            double a, b;
            bool empty = false, dobparse = false;
            if (textBox1.Text == "")
            {
                empty = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }

            if (!empty)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!empty && !dobparse)
                {
                    b = Math.Sin(a);
                    textBox3.Text = b.ToString();
                }
            }
        }

        private void buttoncos_Click(object sender, EventArgs e)
        {
            double a, b;
            bool empty = false, dobparse = false;
            if (textBox1.Text == "")
            {
                empty = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }

            if (!empty)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!empty && !dobparse)
                {
                    b = Math.Cos(a);
                    textBox3.Text = b.ToString();
                }
            }
        }

        private void buttontan_Click(object sender, EventArgs e)
        {
            double a, b;
            bool empty = false, dobparse = false;
            if (textBox1.Text == "")
            {
                empty = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }

            if (!empty)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!empty && !dobparse)
                {
                    b = Math.Tan(a);
                    textBox3.Text = b.ToString();
                }
            }
        }

        private void buttonctan_Click(object sender, EventArgs e)
        {
            double a, b;
            bool empty = false, dobparse = false;
            if (textBox1.Text == "")
            {
                empty = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }

            if (!empty)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!empty && !dobparse)
                {
                    b = 1/(Math.Tan(a));
                    textBox3.Text = b.ToString();
                }
            }
        }

        private void buttonlog_Click(object sender, EventArgs e)
        {
            double a, b;
            bool empty = false, dobparse = false;
            if (textBox1.Text == "")
            {
                empty = true;
                MessageBox.Show("Prvi broj je prazan.", "Pogreška!");
            }

            if (!empty)
            {
                if (!double.TryParse(textBox1.Text, out a) || textBox1.Text.Contains('.'))
                {
                    dobparse = true;
                    MessageBox.Show("Pogrešan unos prvog borja.", "Pogreška!");
                }
                if (!empty && !dobparse)
                {
                    b = Math.Log10(a);
                    textBox3.Text = b.ToString();
                }
            }
        }
    }
}
