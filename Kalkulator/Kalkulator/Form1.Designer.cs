﻿namespace Kalkulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonplus = new System.Windows.Forms.Button();
            this.buttonminus = new System.Windows.Forms.Button();
            this.buttontimes = new System.Windows.Forms.Button();
            this.buttondiv = new System.Windows.Forms.Button();
            this.buttonsin = new System.Windows.Forms.Button();
            this.buttoncos = new System.Windows.Forms.Button();
            this.buttontan = new System.Windows.Forms.Button();
            this.buttonctan = new System.Windows.Forms.Button();
            this.buttonlog = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonplus
            // 
            this.buttonplus.Location = new System.Drawing.Point(61, 194);
            this.buttonplus.Name = "buttonplus";
            this.buttonplus.Size = new System.Drawing.Size(60, 60);
            this.buttonplus.TabIndex = 13;
            this.buttonplus.Text = "+";
            this.buttonplus.UseVisualStyleBackColor = true;
            this.buttonplus.Click += new System.EventHandler(this.buttonplus_Click);
            // 
            // buttonminus
            // 
            this.buttonminus.Location = new System.Drawing.Point(61, 260);
            this.buttonminus.Name = "buttonminus";
            this.buttonminus.Size = new System.Drawing.Size(60, 60);
            this.buttonminus.TabIndex = 14;
            this.buttonminus.Text = "-";
            this.buttonminus.UseVisualStyleBackColor = true;
            this.buttonminus.Click += new System.EventHandler(this.buttonminus_Click);
            // 
            // buttontimes
            // 
            this.buttontimes.Location = new System.Drawing.Point(61, 326);
            this.buttontimes.Name = "buttontimes";
            this.buttontimes.Size = new System.Drawing.Size(60, 60);
            this.buttontimes.TabIndex = 15;
            this.buttontimes.Text = "*";
            this.buttontimes.UseVisualStyleBackColor = true;
            this.buttontimes.Click += new System.EventHandler(this.buttontimes_Click);
            // 
            // buttondiv
            // 
            this.buttondiv.Location = new System.Drawing.Point(61, 392);
            this.buttondiv.Name = "buttondiv";
            this.buttondiv.Size = new System.Drawing.Size(60, 60);
            this.buttondiv.TabIndex = 16;
            this.buttondiv.Text = "/";
            this.buttondiv.UseVisualStyleBackColor = true;
            this.buttondiv.Click += new System.EventHandler(this.buttondiv_Click);
            // 
            // buttonsin
            // 
            this.buttonsin.Location = new System.Drawing.Point(127, 194);
            this.buttonsin.Name = "buttonsin";
            this.buttonsin.Size = new System.Drawing.Size(60, 60);
            this.buttonsin.TabIndex = 17;
            this.buttonsin.Text = "sin";
            this.buttonsin.UseVisualStyleBackColor = true;
            this.buttonsin.Click += new System.EventHandler(this.buttonsin_Click);
            // 
            // buttoncos
            // 
            this.buttoncos.Location = new System.Drawing.Point(127, 260);
            this.buttoncos.Name = "buttoncos";
            this.buttoncos.Size = new System.Drawing.Size(60, 60);
            this.buttoncos.TabIndex = 18;
            this.buttoncos.Text = "cos";
            this.buttoncos.UseVisualStyleBackColor = true;
            this.buttoncos.Click += new System.EventHandler(this.buttoncos_Click);
            // 
            // buttontan
            // 
            this.buttontan.Location = new System.Drawing.Point(127, 326);
            this.buttontan.Name = "buttontan";
            this.buttontan.Size = new System.Drawing.Size(60, 60);
            this.buttontan.TabIndex = 19;
            this.buttontan.Text = "tan";
            this.buttontan.UseVisualStyleBackColor = true;
            this.buttontan.Click += new System.EventHandler(this.buttontan_Click);
            // 
            // buttonctan
            // 
            this.buttonctan.Location = new System.Drawing.Point(127, 392);
            this.buttonctan.Name = "buttonctan";
            this.buttonctan.Size = new System.Drawing.Size(60, 60);
            this.buttonctan.TabIndex = 20;
            this.buttonctan.Text = "ctan";
            this.buttonctan.UseVisualStyleBackColor = true;
            this.buttonctan.Click += new System.EventHandler(this.buttonctan_Click);
            // 
            // buttonlog
            // 
            this.buttonlog.Location = new System.Drawing.Point(193, 194);
            this.buttonlog.Name = "buttonlog";
            this.buttonlog.Size = new System.Drawing.Size(60, 60);
            this.buttonlog.TabIndex = 21;
            this.buttonlog.Text = "log";
            this.buttonlog.UseVisualStyleBackColor = true;
            this.buttonlog.Click += new System.EventHandler(this.buttonlog_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox1.Location = new System.Drawing.Point(61, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(192, 40);
            this.textBox1.TabIndex = 22;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox2.Location = new System.Drawing.Point(61, 70);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(192, 40);
            this.textBox2.TabIndex = 23;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox3.Location = new System.Drawing.Point(61, 125);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(192, 40);
            this.textBox3.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Broj 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Broj 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Rezultat";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 474);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonlog);
            this.Controls.Add(this.buttonctan);
            this.Controls.Add(this.buttontan);
            this.Controls.Add(this.buttoncos);
            this.Controls.Add(this.buttonsin);
            this.Controls.Add(this.buttondiv);
            this.Controls.Add(this.buttontimes);
            this.Controls.Add(this.buttonminus);
            this.Controls.Add(this.buttonplus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button buttonplus;
        private System.Windows.Forms.Button buttonminus;
        private System.Windows.Forms.Button buttontimes;
        private System.Windows.Forms.Button buttondiv;
        private System.Windows.Forms.Button buttonsin;
        private System.Windows.Forms.Button buttoncos;
        private System.Windows.Forms.Button buttontan;
        private System.Windows.Forms.Button buttonctan;
        private System.Windows.Forms.Button buttonlog;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

