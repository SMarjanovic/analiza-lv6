﻿namespace Vjesala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_rijec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_slovo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonProbaj = new System.Windows.Forms.Button();
            this.tb_nPokusaja = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonNova = new System.Windows.Forms.Button();
            this.tb_probana = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tb_rijec
            // 
            this.tb_rijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_rijec.Location = new System.Drawing.Point(89, 68);
            this.tb_rijec.Name = "tb_rijec";
            this.tb_rijec.ReadOnly = true;
            this.tb_rijec.Size = new System.Drawing.Size(645, 47);
            this.tb_rijec.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rijec:";
            // 
            // tb_slovo
            // 
            this.tb_slovo.Location = new System.Drawing.Point(89, 135);
            this.tb_slovo.MaxLength = 1;
            this.tb_slovo.Name = "tb_slovo";
            this.tb_slovo.Size = new System.Drawing.Size(35, 20);
            this.tb_slovo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Slovo:";
            // 
            // buttonProbaj
            // 
            this.buttonProbaj.Location = new System.Drawing.Point(152, 135);
            this.buttonProbaj.Name = "buttonProbaj";
            this.buttonProbaj.Size = new System.Drawing.Size(75, 23);
            this.buttonProbaj.TabIndex = 4;
            this.buttonProbaj.Text = "Probaj";
            this.buttonProbaj.UseVisualStyleBackColor = true;
            this.buttonProbaj.Click += new System.EventHandler(this.buttonProbaj_Click);
            // 
            // tb_nPokusaja
            // 
            this.tb_nPokusaja.Location = new System.Drawing.Point(152, 196);
            this.tb_nPokusaja.Name = "tb_nPokusaja";
            this.tb_nPokusaja.ReadOnly = true;
            this.tb_nPokusaja.Size = new System.Drawing.Size(75, 20);
            this.tb_nPokusaja.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Preostalo pokusaja:";
            // 
            // buttonNova
            // 
            this.buttonNova.Location = new System.Drawing.Point(49, 308);
            this.buttonNova.Name = "buttonNova";
            this.buttonNova.Size = new System.Drawing.Size(75, 23);
            this.buttonNova.TabIndex = 8;
            this.buttonNova.Text = "Nova igra";
            this.buttonNova.UseVisualStyleBackColor = true;
            this.buttonNova.Click += new System.EventHandler(this.buttonNova_Click);
            // 
            // tb_probana
            // 
            this.tb_probana.Location = new System.Drawing.Point(152, 251);
            this.tb_probana.Name = "tb_probana";
            this.tb_probana.ReadOnly = true;
            this.tb_probana.Size = new System.Drawing.Size(354, 20);
            this.tb_probana.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(68, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Probana slova:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_probana);
            this.Controls.Add(this.buttonNova);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_nPokusaja);
            this.Controls.Add(this.buttonProbaj);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_slovo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_rijec);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_rijec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_slovo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonProbaj;
        private System.Windows.Forms.TextBox tb_nPokusaja;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonNova;
        private System.Windows.Forms.TextBox tb_probana;
        private System.Windows.Forms.Label label4;
    }
}

