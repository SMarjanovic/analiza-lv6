﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesala
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Random rand = new Random();
        int n_pokusaja, n_rijeci;
        string rijec, rijec_skrivena, probana_slova;
        string path = "D:\\data.txt";

        private void buttonNova_Click(object sender, EventArgs e)
        {
            rijec = rijeci[rand.Next(n_rijeci)];
            n_pokusaja = 6;
            probana_slova = "";
            tb_probana.Text = "";
            tb_nPokusaja.Text = n_pokusaja.ToString();
            rijec_skrivena = "";
            tb_rijec.Text = rijec_skrivena;
            for (int i = 0; i < rijec.Length; i++)
                rijec_skrivena += '_';
            for (int i = 0; i < rijec_skrivena.Length; i++)
            {
                tb_rijec.Text += rijec_skrivena[i];
                tb_rijec.Text += "  ";
            }
        }

        private void buttonProbaj_Click(object sender, EventArgs e)
        {
            if(n_pokusaja <= 0)
            {
                MessageBox.Show("Nemate vise pokusaja, pokrenite novu igru.", "Pogreška!");
            }
            else
            {
                if (tb_slovo.Text=="")
                {
                    MessageBox.Show("Niste unijeli slovo!", "Pogreška!");
                }
                else
                {
                    if (probana_slova.Contains(tb_slovo.Text))
                    {
                        MessageBox.Show("Vec ste probali to slovo!", "Pogreška!");
                    }
                    else
                    {
                        if (!(rijec.Contains(tb_slovo.Text)))
                        {
                            n_pokusaja--;
                            probana_slova += tb_slovo.Text;
                            tb_probana.Text = probana_slova;
                            tb_nPokusaja.Text = n_pokusaja.ToString();
                        }
                        else
                        {
                            probana_slova += tb_slovo.Text;
                            tb_probana.Text = probana_slova;
                            string tmp = String.Copy(rijec_skrivena);
                            rijec_skrivena = "";
                            for (int i = 0; i < rijec.Length; i++)
                            {
                                if (rijec[i].ToString()  == tb_slovo.Text)
                                {
                                    rijec_skrivena += tb_slovo.Text;
                                }
                                else if (tmp[i]!='_')
                                {
                                    rijec_skrivena += tmp[i];
                                }
                                else
                                {
                                    rijec_skrivena += '_';
                                }
                            }
                            tb_rijec.Text = "";
                            for (int i = 0; i < rijec_skrivena.Length; i++)
                            {
                                tb_rijec.Text += rijec_skrivena[i];
                                tb_rijec.Text += "  ";
                            }
                            if (rijec == rijec_skrivena)
                            {
                                n_pokusaja = 0;
                                MessageBox.Show("Cestitam, pobjedili ste!", "Super!");
                            }
                        }
                    }
                }
            }
        }

        List<string> rijeci = new List<string>();
        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }
            }
            probana_slova = "";
            tb_probana.Text = "";
            n_rijeci = rijeci.Count;
            rijec = rijeci[rand.Next(n_rijeci)];
            n_pokusaja = 6;
            tb_nPokusaja.Text = n_pokusaja.ToString();
            for (int i = 0; i < rijec.Length; i++)
                rijec_skrivena += '_';
            for (int i = 0; i < rijec_skrivena.Length; i++)
            {
                tb_rijec.Text += rijec_skrivena[i];
                tb_rijec.Text += "  ";
            }
        }
    }
}
